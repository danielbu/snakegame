#pragma once
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include "Button.h"

#define NUM_OF_BUTTONS 3

enum class MenuState { PLAY, OPTIONS, EXIT, MENU };

class Menu
{
public:
	Menu(sf::Vector2f windowSize, sf::Font& font, std::string headerText, std::string firstButtonText, sf::Color headerColor);
	~Menu();

	void Draw(sf::RenderWindow& window);
	MenuState ManageButtons(sf::RenderWindow& window);

private:
	Button _buttons[NUM_OF_BUTTONS];
	sf::Text _header;
	sf::Sound _buttonHoverSound;
};

