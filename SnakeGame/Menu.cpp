#include "Menu.h"

Menu::Menu(sf::Vector2f windowSize, sf::Font& font, std::string headerText, std::string firstButtonText, sf::Color headerColor)
{
    _buttons[0] = Button(firstButtonText, sf::Vector2f(windowSize.x / 2.f, windowSize.y * 3.f / 6.f), sf::Color::White, sf::Color::Transparent, 30, sf::Vector2f(windowSize.x * 2.f / 5.f, windowSize.y / 9.f), font);
    _buttons[1] = Button("OPTIONS", sf::Vector2f(windowSize.x / 2.f, windowSize.y * 4.f / 6.f), sf::Color::White, sf::Color::Transparent, 30, sf::Vector2f(windowSize.x * 2.f / 5.f, windowSize.y / 9.f), font);
    _buttons[2] = Button("EXIT", sf::Vector2f(windowSize.x / 2.f, windowSize.y * 5.f / 6.f), sf::Color::White, sf::Color::Transparent, 30, sf::Vector2f(windowSize.x * 2.f / 5.f, windowSize.y / 9.f), font);
    
    _header.setString(headerText);
    _header.setFont(font);
    _header.setCharacterSize(625 / headerText.length());
    _header.setFillColor(headerColor);
    _header.setPosition(windowSize.x / 2.f, windowSize.y / 4.f);
    _header.setOrigin(_header.getLocalBounds().left + _header.getLocalBounds().width / 2.0f, _header.getLocalBounds().top + _header.getLocalBounds().height / 2.0f);
}

Menu::~Menu()
{
}

void Menu::Draw(sf::RenderWindow& window)
{
    for (int i = 0; i < NUM_OF_BUTTONS; i++) {
        _buttons[i].Draw(window);
    }

    window.draw(_header);
}

MenuState Menu::ManageButtons(sf::RenderWindow& window)
{
    for (int i = 0; i < NUM_OF_BUTTONS; i++) {
        if (_buttons[i].ManageButton(window) && i == 0)
            return MenuState::PLAY;
        else if (_buttons[i].ManageButton(window) && i == 1)
            return MenuState::OPTIONS;
        else if (_buttons[i].ManageButton(window) && i == 2)
            return MenuState::EXIT;
    }

    return MenuState::MENU;
}
