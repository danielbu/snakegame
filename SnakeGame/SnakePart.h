#pragma once
#include <SFML/Graphics.hpp>

enum class Direction { UP, DOWN, LEFT, RIGHT };

class SnakePart
{
public:
	SnakePart(float length, sf::Color color, sf::Vector2f position, Direction dir = Direction::RIGHT);
	~SnakePart();

	void Draw(sf::RenderWindow& window);
	void Move(float velocity);
	void Move(sf::Vector2f position) { _body.setPosition(position); }

	//direction setter + getter
	void SetDirection(Direction direction) { this->_direction = direction; }
	Direction GetDirection() { return this->_direction; }

	sf::Vector2f GetPosition() { return this->_body.getPosition(); }

private:
	sf::RectangleShape _body;
	Direction _direction;
};

